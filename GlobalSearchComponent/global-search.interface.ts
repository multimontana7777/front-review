import { LockerBlock, ParkingLot, Poi, Room, User, Workplace } from '@smartoffice-frontend-ws/shared/utils/interfaces';

export interface IGlobalSearchResult {
  ROOM: Room[];
  LABEL: User[];
  WORKPLACE: Workplace[];
  PARKING_LOT: ParkingLot[];
  POI: Poi[];
  LOCKER_BLOCK: LockerBlock[];
  resultCount: number
}
