import { Inject, Injectable } from '@angular/core';
import {
  DictionariesApiService,
  FileBlobApiService,
  V1ApiService,
} from '@smartoffice-frontend-ws/client/data-access/api';
import { SEARCH_GROUPS_SORT } from '@smartoffice-frontend-ws/client/utils/tokens';
import { PAGE_SIZE_FOR_VIRTUAL_SCROLL } from '@smartoffice-frontend-ws/shared/utils/constants';
import {
  IBuilding,
  IBuildingResponse,
  IDictionaryFilter,
  IDictionaryPaginator,
  IDictionarySorter,
  IFloorMap,
  IFloorMapResponse,
  IGlobalSearchResult,
  ILabelGroup,
  ILabelGroupResponse,
  ILockerBlock,
  ILockerBlockResponse,
  IParkingLot,
  IParkingLotResponse,
  IPoi,
  IPoiResponse,
  IRoom,
  IRoomResponse,
  ITag,
  ITagResponse,
  IUser,
  IUserResponse,
  IWorkplace,
  IWorkplaceResponse,
  LockerBlock,
  ObjectType,
  ParkingLot,
  Poi,
  Room,
  User,
  Workplace,
} from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { combineLatest, map, Observable, of, ReplaySubject, switchMap } from 'rxjs';
import { AuthUserService } from './auth-user.service';
import { GlobalStateService } from './global-state.service';
import { LicenseService } from './license.service';

@Injectable()
export class GlobalSearchService {
  public searchStr: string;
  public readonly reload$: ReplaySubject<null> = new ReplaySubject<null>(1);
  public readonly results$: Observable<IGlobalSearchResult | null> = combineLatest([
    this.reload$,
    this.globalStateService.reload$,
  ]).pipe(
    switchMap(() => {
      if (!this.searchStr) {
        return of(null);
      }
      return combineLatest([
        this.searchGroupSort.includes('ROOM')
          ? this.dictionariesApiService.getManyByFilter<IRoomResponse, IRoom>(
              'rooms',
              this.placeFilter,
              this.roomSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
        this.searchGroupSort.includes('LABEL')
          ? this.dictionariesApiService.getManyByFilter<IUserResponse, IUser>(
              'labels',
              this.userFilter,
              this.userSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
        this.searchGroupSort.includes('WORKPLACE')
          ? this.dictionariesApiService.getManyByFilter<IWorkplaceResponse, IWorkplace>(
              'workplaces',
              this.placeFilter,
              this.workplaceSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
        this.searchGroupSort.includes('PARKING_LOT')
          ? this.dictionariesApiService.getManyByFilter<IParkingLotResponse, IParkingLot>(
              'parkingLots',
              this.placeFilter,
              this.parkingLotSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
        this.searchGroupSort.includes('POI')
          ? this.dictionariesApiService.getManyByFilter<IPoiResponse, IPoi>(
              'pois',
              this.placeFilter,
              this.poiSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
        this.searchGroupSort.includes('LOCKER_BLOCK')
          ? this.dictionariesApiService.getManyByFilter<ILockerBlockResponse, ILockerBlock>(
              'lockerBlocks',
              this.placeFilter,
              this.lockerBlockSorter,
              this.placePaginator,
              true,
              false
            )
          : of([]),
      ]).pipe(
        switchMap(([rooms, labels, workplaces, parkingLots, pois, lockerBlocks]) => {
          const labelGroupsIds = [...new Set(labels.map((i) => i.labelGroups).flat())];
          const labelGroupFilter: IDictionaryFilter<ILabelGroupResponse> = {
            disjunction: true,
            matchers: [
              {
                field: 'id',
                condition: 'in',
                value: labelGroupsIds,
              },
            ],
          };
          const bindedParkingLotFilter: IDictionaryFilter<IParkingLotResponse> = {
            disjunction: false,
            matchers: [
              {
                field: 'labelBindings',
                condition: 'intersects',
                value: labels.map((i) => i.id),
              },
            ],
          };
          const bindedWorkplaceFilter: IDictionaryFilter<IWorkplaceResponse> = {
            disjunction: false,
            matchers: [
              {
                field: 'labelBindings',
                condition: 'intersects',
                value: labels.map((i) => i.id),
              },
            ],
          };
          const tagIds = [
            ...new Set([
              ...workplaces.map((i) => i.tags).flat(),
              ...parkingLots.map((i) => i.tags).flat(),
              ...rooms.map((i) => i.tags).flat(),
            ]),
          ];
          const tagFilter: IDictionaryFilter<ITagResponse> = {
            disjunction: false,
            matchers: [
              {
                field: 'id',
                condition: 'in',
                value: tagIds,
              },
            ],
          };
          const floorMapIds = [
            ...new Set([
              ...workplaces.map((i) => i.floorMapId),
              ...parkingLots.map((i) => i.floorMapId),
              ...lockerBlocks.map((i) => i.floorMapId),
              ...pois.map((i) => i.floorMapId),
              ...rooms.filter((i) => i.floorMapId).map((i) => i.floorMapId),
            ]),
          ].filter((i) => i > 0);
          const floorMapFilter: IDictionaryFilter<IFloorMapResponse> = {
            disjunction: false,
            matchers: [
              {
                field: 'id',
                condition: 'in',
                value: floorMapIds,
              },
            ],
          };
          const bindedLabelIds = [
            ...new Set([
              ...workplaces.map((i) => i.labelBindings).flat(),
              ...parkingLots.map((i) => i.labelBindings).flat(),
            ]),
          ];
          const bindedLabelsFilter: IDictionaryFilter<IUserResponse> = {
            disjunction: false,
            matchers: [
              {
                field: 'id',
                condition: 'in',
                value: bindedLabelIds,
              },
            ],
          };
          return combineLatest([
            this.v1ApiService.getLabelStatus(
              { labelIds: labels.map((i) => i.id), dateTimeDate: new Date() },
              true,
              false
            ),
            labelGroupsIds.length
              ? this.dictionariesApiService.getManyByFilter<ILabelGroupResponse, ILabelGroup>(
                  'labelGroups',
                  labelGroupFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            labels.length
              ? this.dictionariesApiService.getManyByFilter<IParkingLotResponse, IParkingLot>(
                  'parkingLots',
                  bindedParkingLotFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            labels.length
              ? this.dictionariesApiService.getManyByFilter<IWorkplaceResponse, IWorkplace>(
                  'workplaces',
                  bindedWorkplaceFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            tagIds.length
              ? this.dictionariesApiService.getManyByFilter<ITagResponse, ITag>(
                  'tags',
                  tagFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            bindedLabelIds.length
              ? this.dictionariesApiService.getManyByFilter<IUserResponse, IUser>(
                  'floorMaps',
                  bindedLabelsFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            floorMapIds.length
              ? this.dictionariesApiService.getManyByFilter<IFloorMapResponse, IFloorMap>(
                  'floorMaps',
                  floorMapFilter,
                  undefined,
                  undefined,
                  true,
                  false
                )
              : of([]),
            rooms.length
              ? combineLatest(rooms.map((i) => this.fileBlobApiService.getImageBlobUrl(i.avatarImageUrl, true, false)))
              : of([]),
            labels.length
              ? combineLatest(labels.map((i) => this.fileBlobApiService.getImageBlobUrl(i.avatarImageUrl, true, false)))
              : of([]),
            lockerBlocks.length
              ? combineLatest(
                  lockerBlocks.map((i) => this.fileBlobApiService.getImageBlobUrl(i.avatarImageUrl, true, false))
                )
              : of([]),
            pois.length
              ? combineLatest(pois.map((i) => this.fileBlobApiService.getImageBlobUrl(i.mapImageUrl, true, false)))
              : of([]),
          ]).pipe(
            switchMap(
              ([
                labelStatuses,
                labelGroups,
                bindedParkingLots,
                bindedWorkplaces,
                tags,
                bindedLabels,
                floorMaps,
                roomsAvatars,
                labelsAvatars,
                lockerBlocksAvatars,
                poisMapImages,
              ]) => {
                const buildingIds = [...new Set([...floorMaps.map((i) => i.buildingId)])];
                const buildingFilter: IDictionaryFilter<IBuildingResponse> = {
                  disjunction: false,
                  matchers: [
                    {
                      field: 'id',
                      condition: 'in',
                      value: buildingIds,
                    },
                  ],
                };
                return (
                  buildingIds.length
                    ? this.dictionariesApiService.getManyByFilter<IBuildingResponse, IBuilding>(
                        'buildings',
                        buildingFilter,
                        undefined,
                        undefined,
                        true,
                        false
                      )
                    : of([])
                ).pipe(
                  map((buildings) => {
                    const favouriteRoomsIds: number[] =
                      this.authUserService.labelPersonalSettings?.frontend.favouriteRoomsIds || [];
                    const favouriteUsersIds: number[] =
                      this.authUserService.labelPersonalSettings?.frontend.favouriteUsersIds || [];
                    const favouriteWorkplacesIds: number[] =
                      this.authUserService.labelPersonalSettings?.frontend.favouriteWorkplacesIds || [];
                    const favouriteParkingLotsIds: number[] =
                      this.authUserService.labelPersonalSettings?.frontend.favouriteParkingLotsIds || [];
                    return {
                      ROOM: rooms.map((i, index) => {
                        const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                        const building = buildings.find((j) => j.id === floorMap?.buildingId);
                        return new Room(i)
                          .setAvatarImageBlobUrl(roomsAvatars[index])
                          .setInfo(building || null, floorMap || null)
                          .setIsFavourite(favouriteRoomsIds.includes(i.id))
                          .setTagItems(tags.filter((j) => i.tags.includes(j.id)));
                      }),
                      LABEL: labels.map((i, index) => {
                        return new User(i)
                          .setAvatarImageBlobUrl(labelsAvatars[index])
                          .setLabelGroupsManager(labelGroups.filter((j) => i.labelGroups.includes(j.id)))
                          .setIsFavourite(favouriteUsersIds.includes(i.id))
                          .setStatus(labelStatuses.find((j) => j.labelId === i.id)?.status || null)
                          .setWorkplaceBindingsData(bindedWorkplaces.filter((j) => j.labelBindings.includes(i.id)))
                          .setParkingLotBindingsData(bindedParkingLots.filter((j) => j.labelBindings.includes(i.id)));
                      }),
                      WORKPLACE: workplaces.map((i) => {
                        const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                        const building = buildings.find((j) => j.id === floorMap?.buildingId);
                        return new Workplace(i)
                          .setInfo(building || null, floorMap || null)
                          .setIsFavourite(favouriteWorkplacesIds.includes(i.id))
                          .setTagItems(tags.filter((j) => i.tags.includes(j.id)));
                      }),
                      PARKING_LOT: parkingLots.map((i) => {
                        const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                        const building = buildings.find((j) => j.id === floorMap?.buildingId);
                        return new ParkingLot(i)
                          .setInfo(building || null, floorMap || null)
                          .setIsFavourite(favouriteParkingLotsIds.includes(i.id))
                          .setTagItems(tags.filter((j) => i.tags.includes(j.id)));
                      }),
                      POI: pois.map((i, index) => {
                        const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                        const building = buildings.find((j) => j.id === floorMap?.buildingId);
                        return new Poi(i)
                          .setInfo(building || null, floorMap || null)
                          .setImageBlobUrl(poisMapImages[index]);
                      }),
                      LOCKER_BLOCK: lockerBlocks.map((i, index) => {
                        const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                        const building = buildings.find((j) => j.id === floorMap?.buildingId);
                        return new LockerBlock(i)
                          .setInfo(building || null, floorMap || null)
                          .setAvatarImageBlobUrl(lockerBlocksAvatars[index]);
                      }),
                      resultCount:
                        rooms.length +
                        labels.length +
                        workplaces.length +
                        parkingLots.length +
                        pois.length +
                        lockerBlocks.length,
                    };
                  })
                );
              }
            )
          );
        })
      );
    })
  );

  constructor(
    private readonly licenseService: LicenseService,
    private readonly authUserService: AuthUserService,
    private readonly dictionariesApiService: DictionariesApiService,
    private readonly fileBlobApiService: FileBlobApiService,
    private readonly v1ApiService: V1ApiService,
    private readonly globalStateService: GlobalStateService,
    @Inject(SEARCH_GROUPS_SORT) private readonly searchGroupSort: ObjectType[]
  ) {}

  get placeFilter(): IDictionaryFilter<
    IWorkplaceResponse | IParkingLotResponse | IRoomResponse | IPoiResponse | ILockerBlockResponse
  > {
    return {
      disjunction: true,
      matchers: [
        {
          field: 'name',
          condition: 'contains',
          value: this.searchStr,
        },
      ],
    };
  }

  get userFilter(): IDictionaryFilter<IUserResponse> {
    return {
      disjunction: true,
      matchers: [
        {
          field: 'name',
          condition: 'contains',
          value: this.searchStr,
        },
        {
          field: 'mail',
          condition: 'contains',
          value: this.searchStr,
        },
        {
          field: 'mainPhone',
          condition: 'contains',
          value: this.searchStr,
        },
      ],
    };
  }

  get workplaceSorter(): IDictionarySorter<IWorkplaceResponse> {
    const sorter: IDictionarySorter<IWorkplaceResponse> = {
      comparators: [],
    };
    if (this.authUserService.labelPersonalSettings?.frontend?.favouriteWorkplacesIds?.length) {
      sorter.comparators.push({
        field: 'id',
        priorityValues: this.authUserService.labelPersonalSettings.frontend.favouriteWorkplacesIds,
        checkOnlyPriority: true,
      });
    }
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get parkingLotSorter(): IDictionarySorter<IParkingLotResponse> {
    const sorter: IDictionarySorter<IParkingLotResponse> = {
      comparators: [],
    };
    if (this.authUserService.labelPersonalSettings?.frontend?.favouriteParkingLotsIds?.length) {
      sorter.comparators.push({
        field: 'id',
        priorityValues: this.authUserService.labelPersonalSettings.frontend.favouriteWorkplacesIds,
        checkOnlyPriority: true,
      });
    }
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get userSorter(): IDictionarySorter<IUserResponse> {
    const sorter: IDictionarySorter<IUserResponse> = {
      comparators: [],
    };
    if (this.authUserService.labelPersonalSettings?.frontend?.favouriteUsersIds?.length) {
      sorter.comparators.push({
        field: 'id',
        priorityValues: this.authUserService.labelPersonalSettings.frontend.favouriteUsersIds,
        checkOnlyPriority: true,
      });
    }
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get roomSorter(): IDictionarySorter<IRoomResponse> {
    const sorter: IDictionarySorter<IRoomResponse> = {
      comparators: [],
    };
    if (this.authUserService.labelPersonalSettings?.frontend?.favouriteRoomsIds?.length) {
      sorter.comparators.push({
        field: 'id',
        priorityValues: this.authUserService.labelPersonalSettings.frontend.favouriteRoomsIds,
        checkOnlyPriority: true,
      });
    }
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get poiSorter(): IDictionarySorter<IPoiResponse> {
    const sorter: IDictionarySorter<IPoiResponse> = {
      comparators: [],
    };
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get lockerBlockSorter(): IDictionarySorter<ILockerBlockResponse> {
    const sorter: IDictionarySorter<ILockerBlockResponse> = {
      comparators: [],
    };
    sorter.comparators.push({ field: 'name', order: 'asc' });
    return sorter;
  }

  get placePaginator(): IDictionaryPaginator {
    return { page: 1, size: PAGE_SIZE_FOR_VIRTUAL_SCROLL };
  }
}
