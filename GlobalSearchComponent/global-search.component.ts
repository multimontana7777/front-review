import { takeUntil } from 'rxjs';
import { ChangeDetectionStrategy, Component, Inject, Injector, ElementRef, ViewChild, OnInit } from '@angular/core';
import {
  LOCKER_AVATAR,
  PARKING_LOT_AVATAR,
  SEARCH_GROUPS_SORT,
  WORKPLACE_AVATAR,
} from '@smartoffice-frontend-ws/client/utils/tokens';
import {
  IGlobalSearchResult,
  ISearchMark,
  LockerBlock,
  ObjectType,
  ParkingLot,
  Poi,
  Room,
  User,
  Workplace,
} from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { GlobalSearchService, MapService } from '@smartoffice-frontend-ws/client/data-access/services';
import { ngDebounce } from '@smartoffice-frontend-ws/client/utils/decorators';
import { DEBOUNCE_MS, OBJECT_TYPE_ICONS } from '@smartoffice-frontend-ws/shared/utils/constants';
import { FAV, ONLINE_MEETING } from '@smartoffice-frontend-ws/client/utils/assets';
import { TranslatePipe } from '@ngx-translate/core';
import { determinePluralSuffix } from '@smartoffice-frontend-ws/shared/utils/converters';

@Component({
  selector: 'sows-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [GlobalSearchService],
})
export class GlobalSearchComponent extends BaseComponent implements OnInit {
  public FAV = FAV;
  public ONLINE_MEETING = ONLINE_MEETING;
  public categories: { name: string; value: string; icon: string }[] = [];
  public selectedCategory: { name: string; value: string; icon: string };
  public categorySelectOpen = false;
  public inputFocused = false;
  public results: IGlobalSearchResult | null;
  private OBJECT_TYPE_ICONS = OBJECT_TYPE_ICONS;

  @ViewChild('inputField') inputField: ElementRef<HTMLInputElement>;

  constructor(
    protected override injector: Injector,
    private readonly mapService: MapService,
    private readonly translatePipe: TranslatePipe,
    public readonly globalSearchService: GlobalSearchService,
    @Inject(SEARCH_GROUPS_SORT) private readonly searchGroupSort: ObjectType[],
    @Inject(PARKING_LOT_AVATAR) private readonly parkingLotAvatar: string,
    @Inject(WORKPLACE_AVATAR) private readonly workplaceAvatar: string,
    @Inject(LOCKER_AVATAR) private readonly lockerAvatar: string
  ) {
    super(injector);
    this.setCategoryOptions();
  }

  ngOnInit(): void {
    this.globalSearchService.results$.pipe(takeUntil(this.notifier$)).subscribe((results) => {
      this.results = results;
      this.inputFocused = true;
      this.cdr.detectChanges();
    });
  }

  public toggleCategorySelect(): void {
    this.categorySelectOpen = !this.categorySelectOpen;
  }

  public reset(): void {
    this.categorySelectOpen = false;
    this.inputFocused = false;
    this.inputFocused = false;
  }

  public clearInput(): void {
    this.globalSearchService.searchStr = '';
    this.inputField.nativeElement.focus();
    this.reload();
  }

  public setSelectedCategory(category: { name: string; value: string; icon: string }): void {
    this.selectedCategory = category;
    this.categorySelectOpen = false;
    this.inputField.nativeElement.focus();
  }

  public getAvatarByObjectType(object: Room | Poi | LockerBlock, objectType: ObjectType): string {
    switch (objectType) {
      case 'PARKING_LOT':
        return this.parkingLotAvatar;
      case 'ROOM':
        return (object as Room).avatarImageBlobUrl || this.ONLINE_MEETING;
      case 'POI':
        return (object as Poi).imageBlobUrl!;
      case 'LOCKER_BLOCK':
        return (object as LockerBlock).avatarImageBlobUrl || this.lockerAvatar;
      case 'WORKPLACE':
      default:
        return this.workplaceAvatar;
    }
  }

  public handlePlaceItemClick(place: Room | Poi | LockerBlock | Workplace | ParkingLot, objectType: ObjectType): void {
    this.linkMap(place.id, objectType);
  }

  public handleUserItemClick(user: User): void {
    const mark: ISearchMark = {
      item: user,
      zoom: false,
      card: true,
      init: true,
      simpleMode: false,
    };
    this.mapService.pulsedFloorItem$.next(mark);
  }

  public getResultText(count: number): string {
    const activeSuffix = determinePluralSuffix(count);
    console.log(count, activeSuffix);
    
    const text = `${this.translatePipe.transform(`declination.found.${activeSuffix}`)} ${count} ${this.translatePipe.transform(`declination.result.${activeSuffix}`)}`;
    return text;
  }

  @ngDebounce(100)
  public handleBlur(): void {
    this.inputFocused = false;
    this.inputFocused = false;
  }

  @ngDebounce(DEBOUNCE_MS)
  public reload(): void {
    this.globalSearchService.reload$.next(null);
  }

  private setCategoryOptions(): void {
    const categories = this.searchGroupSort.map((type) => ({
      name: 'Object types.' + type,
      value: String(type),
      icon: this.OBJECT_TYPE_ICONS[type],
    }));
    categories.unshift({
      name: 'All objects',
      value: '',
      icon: 'bars',
    });
    this.categories = categories;
    this.selectedCategory = categories[0];
  }

  @ngDebounce(100)
  private linkMap(id: number, objectType: ObjectType): void {
    this.router.navigate(['/'], {
      skipLocationChange: true,
      queryParams: { type: objectType, id },
    });
  }
}
