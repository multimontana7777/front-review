import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { determinePluralSuffix } from '@smartoffice-frontend-ws/shared/utils/converters';
import { Appointment, Room } from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { AppointmentEditorService } from '../../services/appointment-editor.service';
import { BottomDrawerService } from '@smartoffice-frontend-ws/client/ui/renderers';

@Component({
  selector: 'sows-appointment-editor-booked',
  templateUrl: './appointment-editor-booked.component.html',
  styleUrls: ['./appointment-editor-booked.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorBookedComponent extends BaseComponent {
  @Input() virtualConferenceHeight: number;
  @Input() bookedRooms: Room[];
  @Input() appointment: Appointment | null;
  @Input() mobileVersion = false;

  constructor(
    private readonly appointmentEditorService: AppointmentEditorService,
    private readonly bottomDrawerService: BottomDrawerService,
    protected override injector: Injector
  ) {
    super(injector);
  }

  get contentHeight(): number {
    return this.layoutHeight - 255 - this.virtualConferenceHeight;
  }

  public deleteRoom(room: Room): void {
    const index = this.appointmentEditorService.memberRooms.findIndex((i) => i.id === room.id);
    if (index !== -1) {
      this.appointmentEditorService.memberRooms.splice(index, 1);
      this.appointmentEditorService.reloadMemberRooms$.next(null);
      this.appointmentEditorService.hasChanges = true;
      this.appointmentEditorService.reloadCheck$.next(null);
    }

    if (this.mobileVersion) {
      this.bottomDrawerService.closeLastDrawerManually();
    }
  }

  public getPeopleSuffix(seatCount: number): string {
    return determinePluralSuffix(seatCount);
  }

  public showOnMap(id: number): void {
    this.router.navigate(['/'], {
      skipLocationChange: true,
      queryParams: { type: 'ROOM', id },
    });
  }
}
