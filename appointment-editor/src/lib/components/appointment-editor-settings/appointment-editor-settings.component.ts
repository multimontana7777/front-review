import { ChangeDetectionStrategy, Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AuthUserService, SettingsService } from '@smartoffice-frontend-ws/client/data-access/services';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { APPOINTMENT_EDITOR_ANGULAR_EDITOR_CONFIG } from '@smartoffice-frontend-ws/shared/utils/constants';
import { disableDate, isSameDay } from '@smartoffice-frontend-ws/shared/utils/converters';
import { Appointment, IPutAppointmentRequest, User } from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { takeUntil } from 'rxjs';
import { AppointmentEditorService } from '../../services/appointment-editor.service';
import { BottomDrawerService } from '@smartoffice-frontend-ws/client/ui/renderers';
import { TranslatePipe } from '@ngx-translate/core';

@Component({
  selector: 'sows-appointment-editor-settings',
  templateUrl: './appointment-editor-settings.component.html',
  styleUrls: ['./appointment-editor-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorSettingsComponent extends BaseComponent implements OnInit {
  public editorConfig = APPOINTMENT_EDITOR_ANGULAR_EDITOR_CONFIG;
  public bodyEditorShow = false;
  public body: string;

  @Input() appointment: Appointment | null;
  @Input() exceptionMessages: string[];
  @ViewChild('bodyEditorTpl') bodyEditorTpl: TemplateRef<any>;

  constructor(
    protected override injector: Injector,
    private readonly settingsService: SettingsService,
    private readonly authUserService: AuthUserService,
    private readonly domSanitizer: DomSanitizer,
    public readonly appointmentEditorService: AppointmentEditorService,
    private readonly bottomDrawerService: BottomDrawerService,
    private readonly translatePipe: TranslatePipe
  ) {
    super(injector);
  }

  get requestBody(): SafeHtml {
    return this.putAppointmentRequest?.body
      ? this.domSanitizer.bypassSecurityTrustHtml(this.putAppointmentRequest.body)
      : '';
  }

  get requestBodyText(): string {
    const html = this.putAppointmentRequest?.body || '';
    return html.replace(/<[^>]+>/g, '').trim();
  }

  get currentUser(): User {
    return this.authUserService.user!;
  }

  get settingsHeight(): number {
    return this.layoutHeight - 100;
  }

  get putAppointmentRequest(): IPutAppointmentRequest {
    return this.appointmentEditorService.putAppointmentRequest!;
  }

  get reservationDurationMinMinutes(): number {
    return this.settingsService.getReservationDurationMinMinutesByObjectType('ROOM');
  }

  get reservationTimeStepMinutes(): number {
    return this.settingsService.getReservationTimeStepMinutesByObjectType('ROOM');
  }

  get workHours(): [[number, number], [number, number]] {
    return this.settingsService.workHours;
  }

  get appointmentDates(): Date[] {
    if (this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate && this.appointmentEditorService.putAppointmentRequest.dateTimeToDate) {
      return [this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate, this.appointmentEditorService.putAppointmentRequest.dateTimeToDate]
    }
    return []
  }

  ngOnInit(): void {
    this.body = this.appointment?.body || this.appointmentEditorService.putAppointmentRequest.body || '';
    this.appointmentEditorService.reloadCheck$.pipe(takeUntil(this.notifier$)).subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  public setDateTimeFromDate(date: Date): void {
    if (!isSameDay(this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate!, date)) {
      this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate?.setFullYear(
        date.getFullYear(),
        date.getMonth(),
        date.getDate()
      );
      this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate = new Date(
        this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate!
      );
      this.appointmentEditorService.putAppointmentRequest.dateTimeToDate?.setFullYear(
        date.getFullYear(),
        date.getMonth(),
        date.getDate()
      );
      this.appointmentEditorService.putAppointmentRequest.dateTimeToDate = new Date(
        this.appointmentEditorService.putAppointmentRequest.dateTimeToDate!
      );
      this.appointmentEditorService.reloadMemberUsers$.next(null);
      this.appointmentEditorService.reloadMemberRooms$.next(null);
      this.appointmentEditorService.reloadCheck$.next(null);
    }
  }

  public setDateTimeFromToDate(dates: [Date, Date]): void {
    this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate = new Date(dates[0]);
    this.appointmentEditorService.putAppointmentRequest.dateTimeToDate = new Date(dates[1]);
    this.appointmentEditorService.reloadCheck$.next(null);
  }

  public disableDateFrom: (date: Date) => boolean = (date: Date): boolean => {
    const maxDateTime = new Date();
    maxDateTime.setMonth(maxDateTime.getMonth() + 60);
    maxDateTime.setHours(23, 59, 59, 999);
    return disableDate(date, new Date(), maxDateTime);
  };

  public closeBodyEditor(): void {
    this.bodyEditorShow = false;
    this.appointmentEditorService.putAppointmentRequest.body = this.body;
  }

  public openBodyEditor(): void {
    if (this.screenSizeService.screenSize === 'SMALL') {
      this.bottomDrawerService.createMobileDrawer(
        this.translatePipe.transform('Meeting description'),
        this.bodyEditorTpl,
        null,
        this.onEditorDrawerClose
      );
    } else {
      this.bodyEditorShow = true;
      this.body = this.appointmentEditorService.putAppointmentRequest.body || this.appointment?.body || '';
    }
  }

  public onDescChange(value: string): void {
    this.appointmentEditorService.putAppointmentRequest.body = value;
    this.onHasChanges();
  }

  public onHasChanges(): void {
    this.appointmentEditorService.hasChanges = true;
  }

  private onEditorDrawerClose = () => {
    this.bottomDrawerService.closeLastDrawerManually();
    this.cdr.detectChanges();
  }
}
