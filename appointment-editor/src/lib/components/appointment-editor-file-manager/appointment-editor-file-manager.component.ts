import { Component, Inject, Injector, Input } from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import { V1ApiService } from '@smartoffice-frontend-ws/client/data-access/api';
import {
  LicenseService,
  NotificationService,
  SettingsService,
} from '@smartoffice-frontend-ws/client/data-access/services';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { MAX_S3_FILE_SIZE_MB } from '@smartoffice-frontend-ws/client/utils/tokens';
import { Appointment } from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { NzUploadChangeParam, NzUploadFile, NzUploadXHRArgs } from 'ng-zorro-antd/upload';
import { Observable, Observer } from 'rxjs';
import { AppointmentEditorService } from '../../services/appointment-editor.service';

@Component({
  selector: 'sows-appointment-editor-file-manager',
  templateUrl: './appointment-editor-file-manager.component.html',
  styleUrls: ['./appointment-editor-file-manager.component.scss'], // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorFileManagerComponent extends BaseComponent {
  @Input() nzShowUploadList = true;
  @Input() appointment: Appointment | null;
  @Input() mobileView = false;

  constructor(
    protected override injector: Injector,
    @Inject(MAX_S3_FILE_SIZE_MB) public readonly maxS3FileSizeMb: number,
    private readonly settingsService: SettingsService,
    private readonly v1ApiService: V1ApiService,
    private readonly translatePipe: TranslatePipe,
    private readonly licenseService: LicenseService,
    private readonly notification: NotificationService,
    public readonly appointmentEditorService: AppointmentEditorService
  ) {
    super(injector);
  }

  get attachmentsAvailable(): boolean {
    const attachments = this.appointment?.attachments?.bindedAttachments;
    if (attachments && attachments.length > 0 || this.appointmentEditorService.newFiles.length > 0) {
      return true;
    }
    return false;
  }

  get downloadUrl(): string {
    return this.licenseService.hostUrl + 's3file/main/';
  }

  get downloadTempUrl(): string {
    return this.licenseService.hostUrl + 's3file/temp/';
  }

  public handleDownload = (item: NzUploadFile) => {
    window.open(item.response.url, '_blank')?.focus();
  };

  public handleChange({ file }: NzUploadChangeParam): void {
    const status = file.status;
    if (status === 'done') {
      this.notification.create(
        'success',
        file.name + ' ' + this.translatePipe.transform('file uploaded successfully'),
        ''
      );
    } else if (status === 'error') {
      this.notification.create(
        'error',
        file.name + ' ' + this.translatePipe.transform('file upload failed'),
        file['message'] || ''
      );
    }
    if (!this.nzShowUploadList) {
      this.appointmentEditorService.newFiles = [...this.appointmentEditorService.newFiles];
    }
    this.appointmentEditorService.hasChanges = true;
    this.cdr.markForCheck();
  }

  public beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]): Observable<boolean> =>
    new Observable((observer: Observer<boolean>) => {
      const isFitSize = file.size! / 1024 / 1024 < this.maxS3FileSizeMb;
      if (!isFitSize) {
        this.notification.create(
          'error',
          this.translatePipe.transform('Maximum allowed file size') +
            ' - ' +
            this.maxS3FileSizeMb +
            this.translatePipe.transform('MB'),
          ''
        );
        observer.complete();
        return;
      }
      observer.next(isFitSize);
      observer.complete();
    });

  public handleUpload = (item: NzUploadXHRArgs) => {
    return this.v1ApiService.s3Upload(item.postFile as File).subscribe({
      next: (res) => {
        if (res?.body?.data) {
          item.onSuccess!({ url: this.downloadTempUrl + res.body.data.id, data: res.body.data }, item.file, null);
        }
      },
      error: (err) => {
        item.onError!(err, item.file);
      },
    });
  };

  public deleteBindedAttachment(index: number) {
    if (Array.isArray(this.appointment?.attachments?.bindedAttachments)) {
      if (!this.appointmentEditorService.putAppointmentRequest.attachments) {
        this.appointmentEditorService.putAppointmentRequest.attachments = { insert: [], delete: [] };
      }
      this.appointmentEditorService.putAppointmentRequest.attachments.delete.push(
        this.appointment!.attachments.bindedAttachments[index].id
      );
      this.appointment!.attachments.bindedAttachments.splice(index, 1);
      this.appointmentEditorService.hasChanges = true;
      this.cdr.markForCheck();
    }
  }
}
