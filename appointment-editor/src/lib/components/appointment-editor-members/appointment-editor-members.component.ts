import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  Injector,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import {
  FilteredRoomsWithPaginationService,
  FilteredUsersWithPaginationService,
  NotificationService,
  OfficesService,
  SettingsService,
} from '@smartoffice-frontend-ws/client/data-access/services';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { FAV } from '@smartoffice-frontend-ws/client/utils/assets';
import { ngDebounce } from '@smartoffice-frontend-ws/client/utils/decorators';
import { EMAIL_REGEXP, PAGE_SIZE_FOR_VIRTUAL_SCROLL } from '@smartoffice-frontend-ws/shared/utils/constants';
import { determinePluralSuffix } from '@smartoffice-frontend-ws/shared/utils/converters';
import {
  Appointment,
  AttendeeAnswerType,
  ILabelGroup,
  IOffice,
  IOption,
  IPutAppointmentRequest,
  Room,
  User,
} from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { map, takeUntil, BehaviorSubject } from 'rxjs';
import { AppointmentEditorService } from '../../services/appointment-editor.service';
import { BottomDrawerService } from '@smartoffice-frontend-ws/client/ui/renderers';

const USER_DRAWER_HEIGHT_EXCEPT_CONTAINER = 268;
const USER_DRAWER_HEIGHT_EXCEPT_CONTAINER_MOBILE = 350;
const ROOM_DRAWER_HEIGHT_EXCEPT_CONTAINER = 380;
const ROOM_DRAWER_HEIGHT_EXCEPT_CONTAINER_MOBILE = 347;

@Component({
  selector: 'sows-appointment-editor-members',
  templateUrl: './appointment-editor-members.component.html',
  styleUrls: ['./appointment-editor-members.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorMembersComponent extends BaseComponent implements OnInit, AfterViewInit {
  public FAV = FAV;
  public dragFieldColumns: { dateTimeFromDate: Date; dateTimeToDate?: Date; disabled: boolean }[];
  public hoursFieldColumns: { dateTimeFromDate: Date; dateTimeToDate: Date }[];
  public usersInDrawerShow = false;
  public roomsInDrawerShow = false;
  public offices: IOffice[] = [];
  public labelGroups: ILabelGroup[];
  public isStartDragging = false;
  public isEndDragging = false;
  public isMobileFilterCollapseAcive = false;
  public selectedRoomDetails$: BehaviorSubject<Room[]> = new BehaviorSubject<Room[]>([]);
  @Input() iCalUId: string | null;
  @Input() members: User[] | null = [];
  @Input() rooms: Room[] | null = [];
  @ViewChild('memberUsersCdkVirtualScrollViewport') memberUsersCdkVirtualScrollViewport: CdkVirtualScrollViewport;
  @ViewChild('memberRoomsCdkVirtualScrollViewport') memberRoomsCdkVirtualScrollViewport: CdkVirtualScrollViewport;
  @ViewChild('roomFormGroupContent') roomFormGroupContent: ElementRef<HTMLDivElement>;
  @ViewChild('timetableTpl') timetableTpl: ElementRef;
  @ViewChild('usersSearch') usersSearch: ElementRef;
  @ViewChild('roomsSearch') roomsSearch: ElementRef;
  @ViewChild('addUserDrawerTpl') addUserDrawerTpl: TemplateRef<any>;
  @ViewChild('addRoomDrawerTpl') addRoomDrawerTpl: TemplateRef<any>;
  @ViewChild('roomDetailsTpl') roomDetailsTpl: TemplateRef<any>;

  private _appointment: Appointment | null;

  @Input() set appointment(appointment: Appointment | null) {
    this._appointment = appointment;
    this.initDragField(true);
  }

  get appointment(): Appointment | null {
    return this._appointment;
  }

  constructor(
    protected override injector: Injector,
    public readonly filteredUsersWithPaginationService: FilteredUsersWithPaginationService,
    public readonly filteredRoomsWithPaginationService: FilteredRoomsWithPaginationService,
    private readonly notification: NotificationService,
    private readonly officesService: OfficesService,
    private readonly appointmentEditorService: AppointmentEditorService,
    private readonly settingsService: SettingsService,
    private readonly translatePipe: TranslatePipe,
    private readonly bottomDrawerService: BottomDrawerService
  ) {
    super(injector);
  }

  get externalMails(): string[] {
    return this.appointmentEditorService.externalMails || [];
  }

  get selectedOffice(): IOffice | null {
    return this.offices.find((i) => i.id === this.filteredRoomsWithPaginationService.buildingId) || null;
  }

  get currentVirtualRoom(): Room | null {
    const result = this.rooms?.find((i) => i.type === 'VIRTUAL') || null;
    if (result) {
      this.filteredRoomsWithPaginationService.virtual = false;
    }
    return result;
  }

  private _usersInDrawer: User[] | null = [];

  get usersInDrawer(): User[] | null {
    return this._usersInDrawer;
  }

  @Input() set usersInDrawer(value: User[] | null) {
    if (value) {
      if (this.filteredUsersWithPaginationService.page === 1) {
        this._usersInDrawer = value;
        this.memberUsersCdkVirtualScrollViewport?.scrollToIndex(0);
        this.filteredUsersWithPaginationService.maxItemInRenderedRangeStream = 0;
      } else {
        this._usersInDrawer = [...this._usersInDrawer!, ...value];
      }
      this.cdr.markForCheck();
    }
  }

  private _roomsInDrawer: Room[] | null = [];

  get roomsInDrawer(): Room[] | null {
    return this._roomsInDrawer;
  }

  @Input() set roomsInDrawer(value: Room[] | null) {
    if (value) {
      if (this.filteredRoomsWithPaginationService.page === 1) {
        this._roomsInDrawer = value;
        this.memberRoomsCdkVirtualScrollViewport?.scrollToIndex(0);
        this.filteredUsersWithPaginationService.maxItemInRenderedRangeStream = 0;
      } else {
        this._roomsInDrawer = [...this._roomsInDrawer!, ...value];
      }
      this.cdr.markForCheck();
    }
  }

  get putAppointmentRequest(): IPutAppointmentRequest {
    return this.appointmentEditorService.putAppointmentRequest;
  }

  get virtualRoomCount(): number {
    return this.settingsService.settings?.virtualRoomCount || 0;
  }

  get workHours(): [[number, number], [number, number]] {
    return this.settingsService.workHours;
  }

  get reservationTimeStepMinutes(): number {
    return this.settingsService.getReservationTimeStepMinutesByObjectType('ROOM');
  }

  get userDrawerHeight(): number {
    if (this.screenSizeService.screenSize === 'SMALL') {
      return window.innerHeight - USER_DRAWER_HEIGHT_EXCEPT_CONTAINER_MOBILE - 44;
    }
    return window.innerHeight - USER_DRAWER_HEIGHT_EXCEPT_CONTAINER;
  }

  get roomDrawerHeight(): number {
    if (this.screenSizeService.screenSize === 'SMALL') {
      return (
        window.innerHeight -
        ROOM_DRAWER_HEIGHT_EXCEPT_CONTAINER_MOBILE -
        44 -
        (this.isMobileFilterCollapseAcive ? 128 : 0)
      );
    }
    return window.innerHeight - ROOM_DRAWER_HEIGHT_EXCEPT_CONTAINER;
  }

  get isUsersSearchStrEmail(): boolean {
    return (
      EMAIL_REGEXP.test(this.filteredUsersWithPaginationService.usersSearchStr) &&
      !this.appointmentEditorService.externalMails.includes(this.filteredUsersWithPaginationService.usersSearchStr) &&
      !this.appointmentEditorService.memberUsers.find(
        (i) => i.mail === this.filteredUsersWithPaginationService.usersSearchStr
      )
    );
  }

  get labelGroupOptions(): IOption[] {
    const labelGroupOptions = this.labelGroups.map((group) => ({
      id: group.id,
      name: group.name,
    }));
    labelGroupOptions.unshift({ id: -5000, name: this.translatePipe.transform('Select all') });
    return labelGroupOptions;
  }

  get officeOptions(): IOption[] {
    const officeOptions = this.offices.map((office) => ({
      id: office.id,
      name: office.name,
    }));
    return officeOptions;
  }

  get floorOptions(): IOption[] {
    if (!this.selectedOffice) {
      return [];
    }
    const floorOptions = this.selectedOffice.floorMaps.map((floor) => ({
      id: floor.id,
      name: floor.name,
    }));
    return floorOptions;
  }

  @HostListener('mouseup') @HostListener('touchend') @HostListener('dragend') @HostListener('drop') onMouseup() {
    if (this.isStartDragging || this.isEndDragging) {
      this.isStartDragging = false;
      this.isEndDragging = false;
      if (
        this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate &&
        this.appointmentEditorService.putAppointmentRequest.dateTimeToDate &&
        this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate >
          this.appointmentEditorService.putAppointmentRequest.dateTimeToDate
      ) {
        const buff = new Date(this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate);
        this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate = new Date(
          this.appointmentEditorService.putAppointmentRequest.dateTimeToDate
        );
        this.appointmentEditorService.putAppointmentRequest.dateTimeToDate = buff;
      }
      this.appointmentEditorService.reloadCheck$.next(null);
    }
  }

  ngOnInit(): void {
    this.appointmentEditorService.reloadCheck$.pipe(takeUntil(this.notifier$)).subscribe(() => {
      this.initDragField();
      this.cdr.markForCheck();
    });
    this.officesService.reloadOffices$.next(null);
    this.officesService.offices$
      .pipe(
        takeUntil(this.notifier$),
        map((offices) => {
          return offices
            .map((i) => ({
              ...i,
              floorMaps: i.floorMaps.filter((j) => {
                const loading = j.loading.find((k) => k.type === 'ROOM');
                return !!loading?.total && j.visible;
              }),
            }))
            .filter((i) => {
              return i.visible && i.floorMaps.length;
            });
        })
      )
      .subscribe((offices) => {
        this.offices = offices;
        if (this.offices.length && this.offices[0].floorMaps.length) {
          this.filteredRoomsWithPaginationService.buildingId = this.offices[0].id;
          this.filteredRoomsWithPaginationService.floorMapId = this.offices[0].floorMaps[0].id;
          this.filteredRoomsWithPaginationService.virtual = false;
        } else {
          this.filteredRoomsWithPaginationService.virtual = true;
        }
        this.cdr.markForCheck();
      });
    this.appointmentEditorService.labelGroups$.pipe(takeUntil(this.notifier$)).subscribe((value) => {
      this.labelGroups = value;
      this.cdr.markForCheck();
    });
  }

  ngAfterViewInit(): void {
    if (this.memberUsersCdkVirtualScrollViewport) {
      this.memberUsersCdkVirtualScrollViewport.renderedRangeStream
        .pipe(takeUntil(this.notifier$))
        .subscribe((value) => {
          if (value.end > this.filteredUsersWithPaginationService.maxItemInRenderedRangeStream) {
            this.filteredUsersWithPaginationService.maxItemInRenderedRangeStream = value.end;
            if (value.end >= (this.filteredUsersWithPaginationService.page - 1) * PAGE_SIZE_FOR_VIRTUAL_SCROLL) {
              this.filteredUsersWithPaginationService.page++;
            }
          }
        });
    }
    if (this.memberRoomsCdkVirtualScrollViewport) {
      this.memberRoomsCdkVirtualScrollViewport.renderedRangeStream
        .pipe(takeUntil(this.notifier$))
        .subscribe((value) => {
          if (value.end > this.filteredRoomsWithPaginationService.maxItemInRenderedRangeStream) {
            this.filteredRoomsWithPaginationService.maxItemInRenderedRangeStream = value.end;
            if (value.end >= (this.filteredRoomsWithPaginationService.page - 1) * PAGE_SIZE_FOR_VIRTUAL_SCROLL) {
              this.filteredRoomsWithPaginationService.page++;
            }
          }
        });
    }
  }

  public initDragField(init = false): void {
    if (this.screenSizeService.screenSize === 'SMALL') {
      return;
    }
    this.dragFieldColumns = [];
    this.hoursFieldColumns = [];
    const start = this.appointment
      ? new Date(this.appointment.dateTimeFromDate!)
      : new Date(this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate!);
    start.setHours(this.workHours[0][0], this.workHours[0][1], 0, 0);
    const end = this.appointment
      ? new Date(this.appointment.dateTimeFromDate!)
      : new Date(this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate!);
    if (this.workHours[1][0] === 24) {
      end.setHours(23, 60 - this.reservationTimeStepMinutes, 0, 0);
    } else {
      end.setHours(this.workHours[1][0], this.workHours[1][1], 0, 0);
    }
    while (start.getTime() <= end.getTime()) {
      if (!start.getMinutes()) {
        const hourEnd = new Date(start);
        hourEnd.setHours(hourEnd.getHours() + 1);
        const hourFieldColumn: {
          dateTimeFromDate: Date;
          dateTimeToDate: Date;
        } = {
          dateTimeFromDate: new Date(start),
          dateTimeToDate: new Date(hourEnd),
        };
        this.hoursFieldColumns.push(hourFieldColumn);
      }
      const dragFieldColumn: { dateTimeFromDate: Date; dateTimeToDate?: Date; disabled: boolean } = {
        dateTimeFromDate: new Date(start),
        disabled: new Date() > start,
      };
      start.setMinutes(start.getMinutes() + this.reservationTimeStepMinutes);
      dragFieldColumn.dateTimeToDate = new Date(start);
      this.dragFieldColumns.push(dragFieldColumn);
    }
    if (init) {
      setTimeout(() => {
        this.timetableTpl.nativeElement.scrollLeft = this.leftByAppointment(this.putAppointmentRequest) - 120;
      }, 1000);
    }
  }

  public leftByAppointment(item: { dateTimeFromDate?: Date }): number {
    const start = new Date(item.dateTimeFromDate!);
    start.setHours(0, 0, 0, 0);
    return (item.dateTimeFromDate!.getTime() - start.getTime()) / 1000 / 60 + 15;
  }

  public getPeopleSuffix(seatCount: number): string {
    return determinePluralSuffix(seatCount);
  }

  public leftByAppointmentAbs(item: { dateTimeFromDate?: Date; dateTimeToDate?: Date }): number {
    const date = new Date(
      item.dateTimeFromDate! < item.dateTimeToDate! ? item.dateTimeFromDate! : item.dateTimeToDate!
    );
    const start = new Date(date);
    start.setHours(0, 0, 0, 0);
    return (date.getTime() - start.getTime()) / 1000 / 60 + 15;
  }

  public widthByAppointment(item: { dateTimeFromDate?: Date; dateTimeToDate?: Date }): number {
    return (item.dateTimeToDate!.getTime() - item.dateTimeFromDate!.getTime()) / 1000 / 60;
  }

  public widthByAppointmentAbs(item: { dateTimeFromDate?: Date; dateTimeToDate?: Date }): number {
    return Math.abs(item.dateTimeToDate!.getTime() - item.dateTimeFromDate!.getTime()) / 1000 / 60;
  }

  @ngDebounce('searchFieldDelayMs') reloadRooms(): void {
    const office = this.offices.find((i) => i.id === this.filteredRoomsWithPaginationService.buildingId);
    if (office && !office.floorMaps.map((i) => i.id).includes(this.filteredRoomsWithPaginationService.floorMapId)) {
      this.filteredRoomsWithPaginationService.floorMapId = office.floorMaps[0].id || -5000;
    }
    this.filteredRoomsWithPaginationService.page = 1;
    this.filteredRoomsWithPaginationService.maxItemInRenderedRangeStream = 0;
    this.filteredRoomsWithPaginationService.roomsIdsToSkip = [
      ...this.appointmentEditorService.memberRooms.map((i) => i.id),
      ...this.appointmentEditorService.roomsDisabledIds,
    ];
    this.filteredRoomsWithPaginationService.reloadRooms$.next(null);
  }

  @ngDebounce('searchFieldDelayMs') reloadUsers(): void {
    this.filteredUsersWithPaginationService.page = 1;
    this.filteredUsersWithPaginationService.maxItemInRenderedRangeStream = 0;
    this.filteredUsersWithPaginationService.userIdsToSkip = this.appointmentEditorService.memberUsers.map((i) => i.id);
    this.filteredUsersWithPaginationService.reloadUsers$.next(null);
  }

  public clearUsersSearchStr(): void {
    if (this.filteredUsersWithPaginationService.usersSearchStr) {
      this.filteredUsersWithPaginationService.usersSearchStr = '';
      this.reloadUsers();
    }
  }

  public clearRoomsSearchStr(): void {
    if (this.filteredRoomsWithPaginationService.roomsSearchStr) {
      this.filteredRoomsWithPaginationService.roomsSearchStr = '';
      this.reloadRooms();
    }
  }

  public toggleUsersInDrawerShow(): void {
    this.usersInDrawerShow = !this.usersInDrawerShow;
    if (this.usersInDrawerShow) {
      this.focusUsersSearch();
      this.reloadUsers();
    }
  }

  public toggleRoomsInDrawerShow(): void {
    if (this.offices.length) {
      this.roomsInDrawerShow = !this.roomsInDrawerShow;
      if (this.roomsInDrawerShow) {
        this.focusRoomsSearch();
        this.reloadRooms();
      }
    } else {
      this.notification.create('error', this.translatePipe.transform('No meeting rooms available'), '');
    }
  }

  public openRoomDetails(room: Room): void {
    this.selectedRoomDetails$.next([room]);
    this.bottomDrawerService.createMobileDrawer(
      this.translatePipe.transform('More about the reservation'),
      this.roomDetailsTpl,
      null
    );
  }

  public openAddUserBottomDrawer(): void {
    this.bottomDrawerService.createMobileDrawer(
      this.translatePipe.transform('Add a user'),
      this.addUserDrawerTpl,
      null
    );
    this.reloadUsers();
  }

  public openAddRoomBottomDrawer(): void {
    this.bottomDrawerService.createMobileDrawer(
      this.translatePipe.transform('Add a meeting room'),
      this.addRoomDrawerTpl,
      null
    );
    this.reloadRooms();
  }

  public isUserSelected(user: User): boolean {
    return this.appointmentEditorService.memberUsers.findIndex((member) => member.id === user.id) !== -1;
  }

  public isRoomSelected(room: Room): boolean {
    return this.appointmentEditorService.memberRooms.findIndex((addedRoom) => addedRoom.id === room.id) !== -1;
  }

  public onAddMember(member: User): void {
    const index = this.appointmentEditorService.memberUsers.findIndex((i) => i.id === member.id);
    if (index !== -1) {
      this.deleteMember(member);
    } else {
      this.appointmentEditorService.memberUsers.push(member);
      this.appointmentEditorService.reloadMemberUsers$.next(null);
      this.appointmentEditorService.reloadCheck$.next(null);
    }
    this.reloadUsers();
  }

  public onAddExternalMember(): void {
    const index = this.appointmentEditorService.externalMails.findIndex(
      (i) => i === this.filteredUsersWithPaginationService.usersSearchStr
    );
    if (index !== -1) {
      this.deleteExternalMember(this.filteredUsersWithPaginationService.usersSearchStr);
    } else {
      this.appointmentEditorService.externalMails.push(this.filteredUsersWithPaginationService.usersSearchStr);
    }
    this.filteredUsersWithPaginationService.usersSearchStr = '';
    this.appointmentEditorService.hasChanges = true;
    this.cdr.markForCheck();
  }

  public deleteMember(member: User): void {
    const index = this.appointmentEditorService.memberUsers.findIndex((i) => i.id === member.id);
    if (index !== -1) {
      this.appointmentEditorService.memberUsers.splice(index, 1);
      this.appointmentEditorService.reloadMemberUsers$.next(null);
      this.appointmentEditorService.hasChanges = true;
    }
  }

  public deleteExternalMember(mail: string): void {
    const index = this.appointmentEditorService.externalMails.findIndex((i) => i === mail);
    if (index !== -1) {
      this.appointmentEditorService.externalMails.splice(index, 1);
      this.appointmentEditorService.hasChanges = true;
      this.appointmentEditorService.reloadCheck$.next(null);
    }
    this.cdr.markForCheck();
  }

  public onAddRoom(room: Room): void {
    const index = this.appointmentEditorService.memberRooms.findIndex((i) => i.id === room.id);
    if (index !== -1) {
      this.deleteRoom(room);
    } else {
      if (room.type === 'VIRTUAL') {
        this.filteredRoomsWithPaginationService.virtual = false;
      }
      this.appointmentEditorService.memberRooms.push(room);
      this.appointmentEditorService.reloadMemberRooms$.next(null);
      this.appointmentEditorService.reloadCheck$.next(null);
      this.reloadRooms();
    }
  }

  public deleteRoom(room: Room): void {
    const index = this.appointmentEditorService.memberRooms.findIndex((i) => i.id === room.id);
    if (index !== -1) {
      this.appointmentEditorService.memberRooms.splice(index, 1);
      this.appointmentEditorService.reloadMemberRooms$.next(null);
      this.appointmentEditorService.hasChanges = true;
      this.appointmentEditorService.reloadCheck$.next(null);
    }
  }

  public deleteExternalMail(mail: string): void {
    const index = this.appointmentEditorService.externalMails.findIndex((i) => i === mail);
    if (index !== -1) {
      this.appointmentEditorService.externalMails.splice(index, 1);
      this.appointmentEditorService.hasChanges = true;
      this.appointmentEditorService.reloadCheck$.next(null);
    }
  }

  public onStartStartDragging(): void {
    this.isStartDragging = true;
  }

  public onStartEndDragging(): void {
    this.isEndDragging = true;
  }

  public onSelect(t: { dateTimeFromDate: Date; dateTimeToDate?: Date }): void {
    if (t.dateTimeFromDate < new Date()) {
      return;
    }
    if (this.isStartDragging) {
      this.appointmentEditorService.putAppointmentRequest.dateTimeFromDate = new Date(t.dateTimeFromDate);
    }
    if (this.isEndDragging) {
      this.appointmentEditorService.putAppointmentRequest.dateTimeToDate = new Date(t.dateTimeToDate!);
    }
  }

  public getPopoverContent(labelGroups: ILabelGroup[]): string {
    return labelGroups.map((group) => group.name).join(', ');
  }

  public getPopoverVisibility(labelGroups: ILabelGroup[]): boolean {
    return labelGroups && labelGroups.length > 1;
  }

  public getUserGroupCount(labelGroups: ILabelGroup[]): number {
    return labelGroups.length - 1;
  }

  public getResponseAnswer(user: User): AttendeeAnswerType | null {
    const result = this.appointment?.attendees.find((i) => i.attendeeId === user.mail)?.responseAnswer;
    return result || null;
  }

  @ngDebounce(500)
  private focusRoomsSearch(): void {
    this.roomsSearch?.nativeElement?.focus();
  }

  @ngDebounce(500)
  private focusUsersSearch(): void {
    this.usersSearch?.nativeElement?.focus();
  }
}
