import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Injector,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { Appointment } from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { AppointmentEditorService } from '../../services/appointment-editor.service';

@Component({
  selector: 'sows-appointment-editor-header',
  templateUrl: './appointment-editor-header.component.html',
  styleUrls: ['./appointment-editor-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorHeaderComponent extends BaseComponent {
  @Input() exceptionMessages: string[] = [];
  @Input() appointment: Appointment | null;
  @Input() footerMode = false;
  @Output() private save: EventEmitter<null> = new EventEmitter<null>();
  @Output() private create: EventEmitter<null> = new EventEmitter<null>();
  @Output() private delete: EventEmitter<null> = new EventEmitter<null>();
  @Output() private extend: EventEmitter<null> = new EventEmitter<null>();
  @ViewChild('mobileFooter') mobileFooter: ElementRef<HTMLDivElement>;

  get hasAppointmentStarted(): boolean {
    return this.appointment ? this.appointment.dateTimeFromDate.getTime() < new Date().getTime() : false;
  }

  get footerHeight(): number {
    if (!this.mobileFooter) {
      return 0;
    }
    return this.mobileFooter.nativeElement.clientHeight;
  }

  constructor(
    protected override injector: Injector,
    public readonly appointmentEditorService: AppointmentEditorService
  ) {
    super(injector);
  }

  public onSave(): void {
    this.save.emit(null);
  }

  public onCreate(): void {
    this.create.emit(null);
  }

  public onDelete(): void {
    this.delete.emit(null);
  }

  public onExtend(): void {
    this.extend.emit(null);
  }
}
