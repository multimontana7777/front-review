import { ChangeDetectionStrategy, Component, Injector, Input } from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import { NotificationService } from '@smartoffice-frontend-ws/client/data-access/services';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { Room } from '@smartoffice-frontend-ws/shared/utils/interfaces';

@Component({
  selector: 'sows-appointment-editor-video-conference',
  templateUrl: './appointment-editor-video-conference.component.html',
  styleUrls: ['./appointment-editor-video-conference.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorVideoConferenceComponent extends BaseComponent {
  @Input() virtualConference: Room | null;

  constructor(
    protected override injector: Injector,
    private readonly notification: NotificationService,
    private readonly translatePipe: TranslatePipe
  ) {
    super(injector);
  }

  public copyToClipboard(url: string): void {
    navigator.clipboard.writeText(url);
    this.notification.create(
      'success',
      this.translatePipe.transform('Success'),
      this.translatePipe.transform('Copied to clipboard')
    );
  }
}
