import { HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { DictionariesApiService, ReservationsApiService } from '@smartoffice-frontend-ws/client/data-access/api';
import {
  AuthUserService,
  GlobalStateService,
  SettingsService,
} from '@smartoffice-frontend-ws/client/data-access/services';
import { MODE } from '@smartoffice-frontend-ws/client/utils/tokens';
import {
  Appointment,
  IAppointmentActionResponse,
  IAppointmentFilter,
  IBuilding,
  IBuildingResponse,
  IDictionaryFilter,
  IFloorMap,
  IFloorMapResponse,
  ILabelGroup,
  ILabelGroupResponse,
  IPutAppointmentRequest,
  IRoom,
  IRoomResponse,
  IUser,
  IUserResponse,
  Room,
  StatusesType,
  User,
} from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { catchError, combineLatest, map, Observable, of, ReplaySubject, switchMap } from 'rxjs';

@Injectable()
export class AppointmentEditorService {
  public appointment: Appointment | null;
  public newFiles: NzUploadFile[] = [];
  public putAppointmentRequest: IPutAppointmentRequest; //запрос на редактирование встречи
  public appointmentId: string | null; //id встречи
  public memberUsers: User[]; //пользователи в списке таймлайна
  public externalMails: string[]; //внешние пользователи в списке приглашенных
  public memberRooms: Room[]; //комнаты в списке таймлайна
  public roomsDisabledIds: number[] = []; //id недоступных комнат

  public readonly hasChanges$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  set hasChanges(value: boolean) {
    if (value && this.appointment && this.putAppointmentRequest) {
      let changes = false;
      changes =
        changes ||
        this.appointment.dateTimeFromDate.getTime() !== this.putAppointmentRequest.dateTimeFromDate?.getTime();
      changes =
        changes || this.appointment.dateTimeToDate.getTime() !== this.putAppointmentRequest.dateTimeToDate?.getTime();
      changes = changes || this.appointment.location !== this.putAppointmentRequest.location;
      changes = changes || this.appointment.subject !== this.putAppointmentRequest.subject;
      changes = changes || this.appointment.body !== this.putAppointmentRequest.body;
      changes = changes || !!this.putAppointmentRequest.attachments?.delete?.length;
      changes = changes || !!this.newFiles.filter((i) => i.status !== 'removed').length;
      changes =
        changes ||
        this.appointment.attendees
          .map((i) => i.attendeeId)
          .sort()
          .join() !==
          [...this.memberRooms.map((i) => i.ewsMail), ...this.memberUsers.map((i) => i.mail), ...this.externalMails]
            .sort()
            .join();
      value = changes;
    }
    this.hasChanges$.next(value);
  }

  public readonly reloadAppointment$: ReplaySubject<null> = new ReplaySubject<null>(1);
  public readonly reloadMemberUsers$: ReplaySubject<null> = new ReplaySubject<null>(1);
  public readonly reloadMemberRooms$: ReplaySubject<null> = new ReplaySubject<null>(1);
  public readonly reloadCheck$: ReplaySubject<null> = new ReplaySubject<null>(1);

  public readonly labelGroups$: Observable<ILabelGroup[]> = this.dictionariesApiService.getManyByFilter<
    ILabelGroupResponse,
    ILabelGroup
  >('labelGroups');

  public readonly memberUsers$: Observable<User[]> = combineLatest([
    this.reloadMemberUsers$,
    this.globalStateService.reload$,
  ]).pipe(
    switchMap(() => {
      this.putAppointmentRequest.attendeeIds = this.memberUsers.map((i) => i.mail);
      return combineLatest([
        this.memberUsers.length ? this.reservationsApiService.getAppointments(this.usersAppointmentFilter) : of([]),
      ]).pipe(
        map(([appointments]) => {
          return this.memberUsers.map((i) =>
            i.setDayAppointments(
              appointments.filter((j) => {
                const appointmentLabelMails =
                  j.attendees
                    ?.filter((k) => !k.isResource && k.responseAnswer !== 'DECLINE')
                    .map((i) => i.attendeeId) || [];
                return appointmentLabelMails.includes(i.mail);
              })
            )
          );
        })
      );
    })
  );

  public readonly check$: Observable<IAppointmentActionResponse | HttpErrorResponse> = combineLatest([
    this.reloadCheck$,
    this.globalStateService.reload$,
  ]).pipe(
    switchMap(() => {
      if (!this.putAppointmentRequest) {
        return of(null);
      }
      return this.reservationsApiService
        .getRoomsDisabled(
          {
            labelId: this.authUserService.user!.id,
            dateTimeFromDate: this.putAppointmentRequest.dateTimeFromDate,
            dateTimeToDate: this.putAppointmentRequest.dateTimeToDate,
          },
          false,
          false
        )
        .pipe(
          switchMap((roomsDisabledIds) => {
            this.roomsDisabledIds = roomsDisabledIds;
            this.putAppointmentRequest.roomIds = this.memberRooms.map((i) => i.id);
            this.putAppointmentRequest.attendeeIds = this.memberUsers.map((i) => i.mail);
            this.putAppointmentRequest.attendeeIds = [...this.memberUsers.map((i) => i.mail), ...this.externalMails];
            return this.reservationsApiService
              .checkPutAppointment(this.putAppointmentRequest, false, false)
              .pipe(catchError((error) => of(error)));
          })
        );
    })
  );

  public readonly memberRooms$: Observable<Room[]> = combineLatest([
    this.reloadMemberRooms$,
    this.globalStateService.reload$,
  ]).pipe(
    switchMap(() => {
      return combineLatest([
        this.memberRooms.length ? this.reservationsApiService.getAppointments(this.roomsAppointmentFilter) : of([]),
      ]).pipe(
        map(([appointments]) => {
          return this.memberRooms.map((i) =>
            i.setDayAppointments(
              appointments.filter((j) => {
                const appointmentLabelMails =
                  j.attendees?.filter((k) => k.isResource && k.responseAnswer !== 'DECLINE').map((i) => i.attendeeId) ||
                  [];
                return appointmentLabelMails.includes(i.ewsMail);
              })
            )
          );
        })
      );
    })
  );

  public readonly appointment$: Observable<Appointment | null> = combineLatest([
    this.reloadAppointment$,
    this.globalStateService.reload$,
  ]).pipe(
    switchMap(() => {
      if (this.appointmentId) {
        return this.reservationsApiService.getAppointmentById(this.appointmentId).pipe(
          switchMap((appointment) => {
            if (appointment) {
              const appointmentRoomMails = [
                ...new Set(appointment.attendees?.filter((j) => j.isResource).map((k) => k.attendeeId)),
              ];
              const roomFilter: IDictionaryFilter<IRoomResponse> = {
                disjunction: true,
                matchers: [
                  {
                    field: 'ewsMail',
                    condition: 'in',
                    value: appointmentRoomMails,
                  },
                ],
              };
              const labelMails = [
                ...new Set([
                  appointment.organizerId,
                  ...appointment.attendees?.filter((j) => !j.isResource).map((k) => k.attendeeId),
                ]),
              ];
              const userFilter: IDictionaryFilter<IUserResponse> = {
                disjunction: true,
                matchers: [
                  {
                    field: 'mail',
                    condition: 'in',
                    value: labelMails,
                  },
                ],
              };
              return combineLatest([
                this.dictionariesApiService.getManyByFilter<IUserResponse, IUser>('labels', userFilter),
                this.dictionariesApiService.getManyByFilter<IRoomResponse, IRoom>('rooms', roomFilter),
              ]).pipe(
                switchMap(([labels, rooms]) => {
                  const floorMapIds = [...new Set(rooms.map((i) => i.floorMapId))];
                  const floorMapFilter: IDictionaryFilter<IFloorMapResponse> = {
                    disjunction: false,
                    matchers: [
                      {
                        field: 'id',
                        condition: 'in',
                        value: floorMapIds,
                      },
                    ],
                  };
                  return (
                    floorMapIds.length
                      ? this.dictionariesApiService.getManyByFilter<IFloorMapResponse, IFloorMap>(
                          'floorMaps',
                          floorMapFilter
                        )
                      : of([])
                  ).pipe(
                    switchMap((floorMaps) => {
                      const buildingIds = floorMaps.map((i) => i.buildingId);
                      const buildingFilter: IDictionaryFilter<IBuildingResponse> = {
                        disjunction: false,
                        matchers: [
                          {
                            field: 'id',
                            condition: 'in',
                            value: buildingIds,
                          },
                        ],
                      };
                      return (
                        buildingIds.length
                          ? this.dictionariesApiService.getManyByFilter<IBuildingResponse, IBuilding>(
                              'buildings',
                              buildingFilter
                            )
                          : of([])
                      ).pipe(
                        map((buildings) => {
                          const roomsInst = rooms.map((i) => {
                            const floorMap = floorMaps.find((j) => j.id === i.floorMapId);
                            const building = buildings.find((j) => j.id === floorMap?.buildingId);
                            return new Room(i).setInfo(building || null, floorMap || null);
                          });
                          const usersInst = labels.map((i) => new User(i));
                          const appointmentLabelMails =
                            appointment.attendees?.filter((j) => !j.isResource).map((i) => i.attendeeId) || [];
                          return new Appointment(appointment, this.settingsService)
                            .setOrganizer(usersInst.find((i) => i.mail === appointment.organizerId) || null)
                            .setIsEditable(this.authUserService.user, this.mode)
                            .setAttendeeUsers(usersInst.filter((j) => appointmentLabelMails.includes(j.mail)))
                            .setRooms(roomsInst)
                            .setActions(this.authUserService.user, this.mode);
                        })
                      );
                    })
                  );
                })
              );
            }
            return of(null);
          })
        );
      }
      return of(null);
    })
  );

  constructor(
    @Inject(MODE) public readonly mode: string,
    private readonly globalStateService: GlobalStateService,
    private readonly authUserService: AuthUserService,
    private readonly settingsService: SettingsService,
    private readonly dictionariesApiService: DictionariesApiService,
    private readonly reservationsApiService: ReservationsApiService
  ) {}

  private get roomsAppointmentFilter(): IAppointmentFilter {
    const dateTimeFromDate = new Date(this.putAppointmentRequest.dateTimeFromDate!);
    dateTimeFromDate.setHours(0, 0, 0, 0);
    const dateTimeToDate = new Date(this.putAppointmentRequest.dateTimeFromDate!);
    dateTimeToDate.setHours(23, 59, 59, 999);
    const appointmentStatuses: StatusesType[] = ['NEW', 'CONFIRMED'];
    return {
      dateTimeFromDate,
      dateTimeToDate,
      appointmentTypes: ['RECURRENT_OCCURRENCE', 'RECURRENT_EXCEPTION', 'SINGLE_INSTANCE'],
      isAttendeeResource: true,
      appointmentStatuses,
      attendeeIds: this.memberRooms.map((i) => i.ewsMail),
    };
  }

  private get usersAppointmentFilter(): IAppointmentFilter {
    const dateTimeFromDate = new Date(this.putAppointmentRequest.dateTimeFromDate!);
    dateTimeFromDate.setHours(0, 0, 0, 0);
    const dateTimeToDate = new Date(this.putAppointmentRequest.dateTimeFromDate!);
    dateTimeToDate.setHours(23, 59, 59, 999);
    const appointmentStatuses: StatusesType[] = ['NEW', 'CONFIRMED'];
    return {
      dateTimeFromDate,
      dateTimeToDate,
      appointmentTypes: ['RECURRENT_OCCURRENCE', 'RECURRENT_EXCEPTION', 'SINGLE_INSTANCE'],
      isAttendeeResource: false,
      appointmentStatuses,
      attendeeIds: this.memberUsers.map((i) => i.mail),
    };
  }
}
