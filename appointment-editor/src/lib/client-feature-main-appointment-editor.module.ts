import { CdkFixedSizeVirtualScroll, ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TranslateModule } from '@ngx-translate/core';
import {
  FilteredRoomsWithPaginationService,
  FilteredUsersWithPaginationService,
  OfficesService,
} from '@smartoffice-frontend-ws/client/data-access/services';
import { ClientUiControlsModule } from '@smartoffice-frontend-ws/client/ui/controls';
import { ClientUiRenderersModule } from '@smartoffice-frontend-ws/client/ui/renderers';
import { ClientUtilsPipesModule } from '@smartoffice-frontend-ws/client/utils/pipes';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { AppointmentEditorBookedComponent } from './components/appointment-editor-booked/appointment-editor-booked.component';
import { AppointmentEditorFileManagerComponent } from './components/appointment-editor-file-manager/appointment-editor-file-manager.component';
import { AppointmentEditorHeaderComponent } from './components/appointment-editor-header/appointment-editor-header.component';
import { AppointmentEditorMembersComponent } from './components/appointment-editor-members/appointment-editor-members.component';
import { AppointmentEditorSettingsComponent } from './components/appointment-editor-settings/appointment-editor-settings.component';
import { AppointmentEditorComponent } from './layout/appointment-editor/appointment-editor.component';
import { AppointmentEditorService } from './services/appointment-editor.service';
import { AppointmentEditorVideoConferenceComponent } from './components/appointment-editor-video-conference/appointment-editor-video-conference.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NzButtonModule,
    NzInputModule,
    NzInputNumberModule,
    NzIconModule,
    NzDividerModule,
    NzFormModule,
    FormsModule,
    ClientUiControlsModule,
    NzUploadModule,
    ClientUtilsPipesModule,
    NzTagModule,
    ClientUiRenderersModule,
    NzDrawerModule,
    AngularEditorModule,
    NzAlertModule,
    NzSelectModule,
    CdkFixedSizeVirtualScroll,
    ScrollingModule,
    NzPopoverModule,
    NzSwitchModule,
    NzToolTipModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppointmentEditorComponent,
    AppointmentEditorHeaderComponent,
    AppointmentEditorSettingsComponent,
    AppointmentEditorFileManagerComponent,
    AppointmentEditorBookedComponent,
    AppointmentEditorMembersComponent,
    AppointmentEditorVideoConferenceComponent,
  ],
  exports: [AppointmentEditorComponent],
  providers: [
    OfficesService,
    AppointmentEditorService,
    FilteredUsersWithPaginationService,
    FilteredRoomsWithPaginationService,
  ],
})
export class ClientFeatureMainAppointmentEditorModule {}
