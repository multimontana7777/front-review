import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import { ReservationsApiService } from '@smartoffice-frontend-ws/client/data-access/api';
import {
  AuthUserService,
  FilteredRoomsWithPaginationService,
  FilteredUsersWithPaginationService,
  NotificationService,
  SettingsService,
} from '@smartoffice-frontend-ws/client/data-access/services';
import { BaseComponent } from '@smartoffice-frontend-ws/client/ui/base';
import { Appointment, IPutAppointmentRequest, Room, User } from '@smartoffice-frontend-ws/shared/utils/interfaces';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Observable, take, takeUntil } from 'rxjs';
import { AppointmentEditorService } from '../../services/appointment-editor.service';
import { AppointmentEditorHeaderComponent } from '../../components/appointment-editor-header/appointment-editor-header.component';

@Component({
  selector: 'sows-appointment-editor',
  templateUrl: './appointment-editor.component.html',
  styleUrls: ['./appointment-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppointmentEditorComponent extends BaseComponent implements OnInit {
  public rooms: Room[];
  public members: User[];
  public exceptionMessages: string[] = [];

  public readonly usersInDrawer$: Observable<User[]> = this.filteredUsersWithPaginationService.users$;
  public readonly roomsInDrawer$: Observable<Room[]> = this.filteredRoomsWithPaginationService.rooms$;

  @Output() private onHasChanges: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('vc') vc: ElementRef;
  @ViewChild(AppointmentEditorHeaderComponent) editorHeader: AppointmentEditorHeaderComponent;

  get currentVirtualRoom(): Room | null {
    return this.rooms?.find((i) => i.type === 'VIRTUAL') || null;
  }

  get vcHeight(): number {
    return this.vc?.nativeElement?.offsetHeight || 0;
  }

  constructor(
    protected override injector: Injector,
    private readonly appointmentEditorService: AppointmentEditorService,
    private readonly reservationsApiService: ReservationsApiService,
    private readonly settingsService: SettingsService,
    private readonly authUserService: AuthUserService,
    private readonly filteredUsersWithPaginationService: FilteredUsersWithPaginationService,
    private readonly filteredRoomsWithPaginationService: FilteredRoomsWithPaginationService,
    private readonly translatePipe: TranslatePipe,
    private readonly modal: NzModalService,
    private readonly notification: NotificationService
  ) {
    super(injector);
  }

  get footerHeight(): number {
    if (this.editorHeader) {
      return this.editorHeader.footerHeight;
    }
    return 0;
  }

  get appointment(): Appointment | null {
    return this.appointmentEditorService.appointment;
  }

  get id(): string | null {
    return this.appointmentEditorService.appointmentId;
  }

  @Input() set id(value: string | null) {
    if (value) {
      value = value.replace(/%(25)+/g, '%');
      value = decodeURIComponent(value);
    }
    this.appointmentEditorService.appointmentId = value;
    this.appointmentEditorService.reloadAppointment$.next(null);
  }

  get putAppointmentRequest(): IPutAppointmentRequest | null {
    return this.appointmentEditorService.putAppointmentRequest || null;
  }

  ngOnInit(): void {
    this.appointmentEditorService.hasChanges$
      .pipe(takeUntil(this.notifier$))
      .subscribe((result) => this.onHasChanges.emit(result));
    this.appointmentEditorService.appointment$.pipe(takeUntil(this.notifier$)).subscribe({
      next: (value) => {
        this.exceptionMessages = [];
        if (value) {
          this.appointmentEditorService.hasChanges = false;
          const { subject, body, location, dateTimeFromDate, dateTimeToDate, reminderMinutesBeforeStart, id } = value;
          this.appointmentEditorService.putAppointmentRequest = {
            appointmentId: id,
            excludedAppointmentId: id,
            body,
            subject,
            location,
            dateTimeFromDate: new Date(dateTimeFromDate),
            dateTimeToDate: new Date(dateTimeToDate),
            reminderMinutesBeforeStart,
            attachments: { insert: [], delete: [] },
          };
          this.appointmentEditorService.appointment = value;
          this.appointmentEditorService.newFiles = [];
          this.appointmentEditorService.memberUsers = [
            ...this.appointmentEditorService.appointment.attendeeUsers!,
            this.appointmentEditorService.appointment.organizerUser!,
          ];
          this.appointmentEditorService.memberRooms = [...this.appointmentEditorService.appointment.rooms];
          this.appointmentEditorService.externalMails = [
            ...this.appointmentEditorService.appointment.attendees
              .filter((i) => !i.isResource)
              .map((i) => i.attendeeId)
              .filter((i) => !this.appointmentEditorService.memberUsers.map((j) => j.mail).includes(i)),
          ];
          this.appointmentEditorService.reloadMemberUsers$.next(null);
          this.appointmentEditorService.reloadMemberRooms$.next(null);
        } else {
          this.appointmentEditorService.hasChanges = true;
          const dateTimeFromDate = new Date();
          dateTimeFromDate.setMinutes(
            Math.ceil(
              dateTimeFromDate.getMinutes() / this.settingsService.getReservationTimeStepMinutesByObjectType('ROOM')
            ) * this.settingsService.getReservationTimeStepMinutesByObjectType('ROOM'),
            0,
            0
          );
          const dateTimeToDate = new Date(dateTimeFromDate);
          dateTimeToDate.setMinutes(
            dateTimeToDate.getMinutes() + this.settingsService.getReservationDurationMinMinutesByObjectType('ROOM')
          );
          this.appointmentEditorService.putAppointmentRequest = {
            body: '',
            subject: this.settingsService.settings!.calendarAppointmentDefaultSubject,
            location: '',
            reminderMinutesBeforeStart: 15,
            dateTimeFromDate: new Date(dateTimeFromDate),
            dateTimeToDate: new Date(dateTimeToDate),
            attachments: { insert: [], delete: [] },
          };
          this.appointmentEditorService.appointment = value;
          this.appointmentEditorService.newFiles = [];
          this.appointmentEditorService.memberUsers = [this.authUserService.user!];
          this.appointmentEditorService.memberRooms = [];
          this.appointmentEditorService.externalMails = [];
          this.appointmentEditorService.reloadMemberUsers$.next(null);
          this.appointmentEditorService.reloadMemberRooms$.next(null);
        }
        this.cdr.markForCheck();
      },
      error: (err) => {
        this.notification.create('error', err.error.RESULT_FAILURE, '');
      },
    });

    this.appointmentEditorService.memberRooms$.pipe(takeUntil(this.notifier$)).subscribe((value) => {
      const isAutoLocation =
        !this.appointmentEditorService.putAppointmentRequest.location ||
        this.appointmentEditorService.putAppointmentRequest.location === this.rooms?.map((i) => i.name)?.join(', ');
      this.rooms = value;
      if (isAutoLocation) {
        this.appointmentEditorService.putAppointmentRequest.location = this.rooms.map((i) => i.name).join(', ');
      }
      this.cdr.markForCheck();
    });
    this.appointmentEditorService.memberUsers$.pipe(takeUntil(this.notifier$)).subscribe((value) => {
      this.members = value.sort((a) => (a.mail === this.appointmentEditorService.appointment?.organizerId ? -1 : 1));
      this.cdr.markForCheck();
    });
    this.appointmentEditorService.check$.pipe(takeUntil(this.notifier$)).subscribe((value) => {
      if (value instanceof HttpErrorResponse) {
        this.exceptionMessages = [value.error.RESULT_FAILURE];
        this.appointmentEditorService.hasChanges = true;
        this.cdr.markForCheck();
      } else {
        if (value && value.meta) {
          this.exceptionMessages = value.meta
            .filter((i) => !i.isOk)
            .map((i) => {
              let message = '';
              const room = this.rooms.find((j) => j.id === i.recordCode);
              if (room) {
                message += room.name + ': ';
              }
              message += i.exception;
              return message;
            });
        }
      }
      this.appointmentEditorService.hasChanges = true;
      this.cdr.markForCheck();
    });
    this.appointmentEditorService.reloadCheck$.next(null);
  }

  public onCreate(): void {
    this.appointmentEditorService.hasChanges = false;
    if (!this.appointmentEditorService.putAppointmentRequest.attachments) {
      this.appointmentEditorService.putAppointmentRequest.attachments = { insert: [], delete: [] };
    }
    this.appointmentEditorService.putAppointmentRequest.attachments.insert = this.appointmentEditorService.newFiles.map(
      (i) => i.response.data.id
    );
    this.appointmentEditorService.putAppointmentRequest.roomIds = this.appointmentEditorService.memberRooms.map(
      (i) => i.id
    );
    this.appointmentEditorService.putAppointmentRequest.attendeeIds = [
      ...this.appointmentEditorService.memberUsers.map((i) => i.mail),
      ...this.appointmentEditorService.externalMails,
    ];
    this.reservationsApiService
      .putAppointment(this.appointmentEditorService.putAppointmentRequest)
      .pipe(take(1))
      .subscribe({
        next: (result) => {
          if (this.screenSizeService.screenSize === 'SMALL') {
            this.globalStateService.reload$.next(null);
            this.globalStateService.openBookingDrawer$.next(null);
            const message = this.translatePipe.transform('Appointment saved successfully');
            this.notification.create('success', this.translatePipe.transform('Success'), message);
          } else {
            this.router.navigate([this.router.url, result.id]);
          }
        },
        error: (err) => {
          this.notification.create('error', err.error.RESULT_FAILURE, '');
        },
      });
  }

  public onSave(): void {
    this.appointmentEditorService.hasChanges = false;
    if (!this.appointmentEditorService.putAppointmentRequest.attachments) {
      this.appointmentEditorService.putAppointmentRequest.attachments = { insert: [], delete: [] };
    }
    this.appointmentEditorService.putAppointmentRequest.attachments.insert = this.appointmentEditorService.newFiles.map(
      (i) => i.response.data.id
    );
    this.appointmentEditorService.putAppointmentRequest.roomIds = this.appointmentEditorService.memberRooms.map(
      (i) => i.id
    );
    this.appointmentEditorService.putAppointmentRequest.attendeeIds = [
      ...this.appointmentEditorService.memberUsers.map((i) => i.mail),
      ...this.appointmentEditorService.externalMails,
    ];
    this.reservationsApiService
      .changeAppointment(this.appointmentEditorService.putAppointmentRequest)
      .pipe(take(1))
      .subscribe({
        next: () => {
          if (this.screenSizeService.screenSize === 'SMALL') {
            this.globalStateService.reload$.next(null);
            this.globalStateService.openBookingDrawer$.next(null);
            const message = this.translatePipe.transform('Appointment saved successfully');
            this.notification.create('success', this.translatePipe.transform('Success'), message);
          } else {
            this.appointmentEditorService.reloadAppointment$.next(null);
          }
        },
        error: (err) => {
          this.notification.create('error', err.error.RESULT_FAILURE, '');
        },
      });
  }

  public onDelete(): void {
    this.modal.confirm({
      nzTitle: this.translatePipe.transform(
        this.appointment?.appointmentStatus === 'CLOSED'
          ? 'Are you sure you want to delete meeting?'
          : 'Are you' + ' sure you want to cancel meeting?'
      ),
      nzOnOk: () => {
        this.appointmentEditorService.hasChanges = false;
        this.reservationsApiService
          .deleteAppointment({ appointmentId: this.appointmentEditorService.putAppointmentRequest.appointmentId! })
          .pipe(take(1))
          .subscribe(() => {
            if (this.screenSizeService.screenSize === 'SMALL') {
              this.globalStateService.reload$.next(null);
              this.globalStateService.openBookingDrawer$.next(null);
            } else {
              this.onBack();
            }
            const message = this.translatePipe.transform(
              this.appointment?.appointmentStatus === 'CLOSED'
                ? 'Appointment successfully deleted'
                : 'Appointment successfully canceled'
            );
            this.notification.create('success', this.translatePipe.transform('Success'), message);
          });
      },
      nzMaskClosable: true,
    });
  }

  public onExtend(): void {
    if (!this.appointmentEditorService.appointment) {
      return;
    }
    this.globalStateService.extendDrawerMark$.next({
      reservation: this.appointmentEditorService.appointment,
      objectType: 'ROOM',
    });
  }

  public onLinkTo(url: string) {
    this.router.navigate([url]);
  }
}
